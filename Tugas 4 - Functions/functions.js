//No. 1
const teriak = () => {
    console.log('-----NO 1-----');
    function teriak(sanbercode = "Halo Sanbers!\n") {
        return sanbercode
    }
    console.log(teriak());
}
teriak();


//No. 2
const kalikan = () => {
    function kalikan(a,b) {
    console.log('-----NO 2-----');
    return a * b;        
    }
    console.log(kalikan(12,4));
}
kalikan();

//No. 3
const intro = () => {
        function introduce(name,age,address,hobby) {
        console.log('\n-----NO 3-----');
        return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`                  
        }
        console.log(introduce("Agus", 30, "Jln. Malioboro, Yogyakarta", "Gaming"));
}
intro();
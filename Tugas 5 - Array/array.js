//No 1 Jarak
const jarak = () => {
    console.log("\n----NO 1----\n")
function range(startNum,finishNum) {
var angka = [];
if(!finishNum) {
    return angka = [-1];
} else if(startNum > finishNum) {
    for(i = startNum; i >= finishNum; i--) {
        angka.push(i);
    }
    return angka;
}
for(i = startNum; i<=finishNum; i++) {
angka.push(i);
}
return angka
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50)) 
console.log(range()) 
console.log(`\n`)
}
jarak();

//No 2 step
const step = () => {
    console.log("\n----NO 2----\n")
function rangeWithStep(startNum,finishNum,step) {
var angka = [];
if(startNum > finishNum) {
    for(i = startNum; i >= finishNum; i -= step) {
        angka.push(i);
    }
    return angka;
}
else { for(i = startNum; i<=finishNum; i += step) {
angka.push(i);
}
return angka
    }
        }
        console.log(rangeWithStep(29, 10, 1)) // [29, 25, 21, 17, 13, 9, 5]
        console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
        console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
        console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
        console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
console.log(`\n`)
}
step();

//Soal No. 3 (Sum of Range)
const sumRange = () => {
    console.log("\n----NO 3----\n")
    function sum(startNum, finishNum, step) {
        var rangearr = [];
        var distance;
        if(!step) {
            distance = 1
        } else {
            distance = step
        }
        if(startNum > finishNum) {
            var currentNum = startNum;
            for(var i = 0; currentNum >= finishNum; i++) {
                rangearr.push(currentNum)
                currentNum -= distance
        }
        } else if (startNum < finishNum) {
            var currentNum = startNum;
            for(var i = 0; currentNum <= finishNum; i++) {
                rangearr.push(currentNum)
                currentNum += distance
        }
    }
            else if (!startNum & !finishNum & !step) {
                return 0
            }
    else if (startNum) {
        return startNum
    }
    var total = 0;
    for (i = 0; i < rangearr.length; i++) {
        total = total + rangearr[i]
    }
    return total
    }
    console.log(sum(1,10)) // 55
    console.log(sum(5, 50, 2)) // 621
    console.log(sum(15,10)) // 75
    console.log(sum(20, 10, 2)) // 90
    console.log(sum(1)) // 1
    console.log(sum()) // 0 
}
sumRange();



const datahand = () => {
    console.log("\n----NO 4----\n")
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 
    
    function dataHandling(data) {
        var datalength = data.length
        for(var i = 0; i < data.length; i++) {
            var id = "Nomor ID:" + data[i][0]
            var nama = "Nama Lengkap:" + data[i][1]
            var ttl = "TTL:" + data[i][2] + " " + data[i][3]
            var hobi = "Hobi:" + data[i][4]
            console.log(id)
            console.log(nama)
            console.log(ttl)
            console.log(hobi)
            console.log("\n-------\n")
        }
    }
    dataHandling(input);
}
datahand();

const balik = () => {
    console.log("\n----NO 5----\n")
    function balikKata(kata) {
        var katabaru = "";
        for(i = kata.length - 1; i >= 0; i--) {
            katabaru += kata[i]
        }
        return katabaru;
    }
    
    console.log(balikKata("Kasur Rusak")) // kasuR rusaK
    console.log(balikKata("SanberCode")) // edoCrebnaS
    console.log(balikKata("Haji Ijah")) // hajI ijaH
    console.log(balikKata("racecar")) // racecar
    console.log(balikKata("I am Sanbers")) // srebnaS ma I 
}
balik();


const metodeArray = () => {
    console.log("\n----NO 6----\n")
    var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
    dataHandling2(input);
    
    function dataHandling2(data) {
        var newData = data
        var newName = data[1] + "Elsharawy"
        var newProvince = "Provinsi" + data[2]
        var gender = "Pria"
        var institusi = "SMA International Metro"
    
        newData.splice(1 , 1, newName)
        newData.splice(2, 1, newProvince)
        newData.splice(4, 1, gender, institusi)
    
        var arrDate = data [3]
        var newDate = arrDate.split('/n')
        var monthNum = newDate [1]
        var monthName = " "
    
        switch(monthNum) {
            case "01": 
            monthName = "Januari"
            break;
            case "02": 
            monthName = "Februari"
            break;
            case "03": 
            monthName = "Maret"
            break;
            case "04": 
            monthName = "April"
            break;
            case "05": 
            monthName = "Mei"
            break;
            case "06": 
            monthName = "Juni"
            break;
            case "07": 
            monthName = "Juli"
            break;
            case "08": 
            monthName = "Agustus"
            break;
            case "09": 
            monthName = "September"
            break;
            case "10": 
            monthName = "Oktober"
            break;
            case "11": 
            monthName = "November"
            break;
            case "12": 
            monthName = "Desember"
            break;
            default:
                break;
        }
    
        var dateJoin = newDate.join('-')
        var dateArr = newDate.sort(function(value1,value2) {
    value2 - value1
        })
    var editName = newName.slice(0,15)
    console.log(newData)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
    
    }
}
metodeArray();
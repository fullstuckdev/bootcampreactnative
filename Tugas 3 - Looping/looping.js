//No. 1 Looping While

const loopingWhile = () => { 
    
console.log(`-----NO 1 LOOPING WHILE-----\n`);

console.log(`LOOPING PERTAMA\n`);
var nomor = 2;
while(nomor <= 20 ) {
    console.log(`${nomor} - I love coding`);
    nomor += 2
} 
console.log(`\nLOOPING KEDUA\n`);
while(nomor > 2 ) {
    nomor -= 2
    console.log(`${nomor} - I will become a mobile developer`);
} 
}
loopingWhile();

//No. 2 Looping menggunakan for

const loopingFor = () => {

console.log(`\n-----NO 2 Looping menggunakan for-----\n`);

    for(var i = 0; i <= 20; i++){
        if (i % 2 != 0 && i % 3 != 0) {
            console.log(`${i} - Santai`);
        }else if(i % 2 == 0 && i != 0 ) {
            console.log(`${i} - Berkualitas`);
        }else if (i % 3 == 0 && i % 2 != 0) {
            console.log(`${i} - I Love Coding`);
        }
    }
}
loopingFor();

//No. 3 Membuat Persegi Panjang #

const persegi = () => {

    console.log(`\n-----No. 3 Membuat Persegi Panjang #-----\n`);

    var p = '';
    for(a = 1; a <=4; a++) {
        for(b = 1; b <=8; b++) {
            p += '#';
        }
        p += '\n';
    }
    console.log(p);
}
persegi();

//No. 4 Membuat Tangga 

const tangga = () => {

    console.log(`\n-----No. 4 Membuat Tangga-----\n`);

    var t = '';
    for(a = 1; a <= 7; a++){
    for(b = 1; b <=a; b ++) {
        t += '#';
    }
    t += '\n';
    }
    console.log(t);
}
tangga();

//No. 5 Membuat Papan Catur
const catur = () => {

    console.log(`\n-----No. 5 Membuat Papan Catur-----\n`);

var g = '';
for (a = 1 ; a <= 8; a++) {
for (b = 1; b <= 4 ; b++) if(a % 2 != 0) { {
    g += " ";
    g += '#'; 
}} else if ( a % 2 == 0) {
    g += '#';
    g += " ";
}
g += '\n';
}
console.log(g);
}
catur();
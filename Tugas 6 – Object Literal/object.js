const arrayKeObject = () => {
    console.log(`\n-------Soal 1------`)
    function arrayToObject(arr){
        if(arr.length <= 0){
            return console.log("")
        }
        for(i = 0; i<arr.length; i++){
            var newObject = {}
            var birthYear = arr[i][3];
            var now = new Date().getFullYear()
            var newAge;
            if(birthYear && now - birthYear > 0){
                newAge = now - birthYear
            }else {
                newAge = "invalid Birth Year"
            }
            newObject.firstName = arr[i][0]
            newObject.lastName = arr[i][1]
            newObject.gender = arr[i][2]
            newObject.age = newAge
    
            var consoleText = (i+1) + ' ' + newObject.firstName + ' ' + newObject.lastName + ' : '
    
            console.log(consoleText)
            console.log(newObject)
        }
    }
    var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
    arrayToObject(people)
    console.log('\n')
}
arrayKeObject();

const shopping = () => {
    console.log(`\n-------Soal 2------`)
    function shoppingTime(memberId, money) {
        if(!memberId) {
            return "Mohon maaf, toko x hanya berlaku untuk member saja"
        } else if (money <50000) {
            return "Mohon maaf, uang tidak cukup"
        }else {
            var newObject = {}
            var moneyChange = money;
            var purchaseList = [];
            var bajuHn = 'Baju HN'
            var sepatuStacattu = 'sepatu stacatu';
            var bajuZoro = 'baju zoro';
            var sweterUniklooh = 'Sweater Uniklooh';
            casingHandphone = 'casing Handphone';
            var check = 0;
            for(i = 0; moneyChange >= 50000 && check == 0; i++){
            if (moneyChange >= 1500000) {
                purchaseList.push(sepatuStacattu)
                moneyChange -= 1500000
            } else if(moneyChange >= 500000){
                purchaseList.push(bajuZoro)
                moneyChange -= 500000
            }else if(moneyChange >= 250000){
                purchaseList.push(bajuHn)
                moneyChange -= 250000
            }else if(moneyChange >= 175000){
                purchaseList.push(sweterUniklooh)
                moneyChange -= 175000
            }else if(moneyChange >= 50000) {
                for(j=0; j <= purchaseList.length-1; j++) {
                    if(purchaseList[j] == casingHandphone) {
                        check += 1
                    }
                }if(check==0) {
                    purchaseList.push(casingHandphone)
                    moneyChange -= 50000
                }else {
                    purchaseList.push(casingHandphone)
                    moneyChange -= 50000
                }
            }
            }
            newObject.memberId = memberId
            newObject.money = money
            newObject.listPurchased = purchaseList
            newObject.changeMoney = moneyChange
            return newObject
        }
        }
        
        console.log(shoppingTime('1820RzKrnWn08', 2475000));
        console.log(shoppingTime('82Ku8Ma742', 170000));
        console.log(shoppingTime('', 2475000));
        console.log(shoppingTime('234JdhweRxa53', 15000));
        console.log(shoppingTime()); 
        console.log(`\n-------Soal 3------`)
}
shopping();

const angkot = () => {
    function naikAngkot(arrPenumpang) {
        var rute = ['A','B','C','D','E','F']
        var arrOutput = []
        if (arrPenumpang.length <=0){
            return []
        }
        for(i = 0; i < arrPenumpang.length; i++) {
            var objOutput = {}
            var asal = arrPenumpang [i][1]
            var tujuan = arrPenumpang [i][2]
    
            var indexAsal;
            var indexTujuan;
    
            for(j = 0; j < rute.length; j++){
                if(rute[j] == asal){
                    indexAsal = j
                }else if(rute [j] == tujuan) {
                    indexTujuan = j
                }
            }
            var bayar = (indexTujuan - indexAsal) * 2000
    
            objOutput.penumpang = arrPenumpang[i][0]
            objOutput.naikDari = asal
            objOutput.tujuan = tujuan
            objOutput.bayar = bayar
            
            arrOutput.push(objOutput)
        }
        return arrOutput
    }
    console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
    
}
angkot()
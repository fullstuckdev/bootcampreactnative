//Soal No. 1 (Membuat kalimat)

const aksesKalimat = () => { 
    var word = 'JavaScript'; 
    var second = 'is'; 
    var third = 'awesome'; 
    var fourth = 'and'; 
    var fifth = 'I'; 
    var sixth = 'love'; 
    var seventh = 'it!';
    
    console.log("---------NO 1----------")
    console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh} \n`);
    console.log("---------NO 2----------")
    
    }
    aksesKalimat();
    
    //Soal No.2 Mengurai kalimat (Akses karakter dalam string)
    const aksesChar = () => {
    var sentence = "I am going to be React Native Developer"; 
    
    var satu = sentence[0] ; 
    var dua = sentence[2] + sentence[3];
    var tiga = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
    var empat = sentence[11] + sentence[12];
    var lima = sentence[14] + sentence[15];
    var enam = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22];
    var tujuh = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
    var delapan = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];
    
    console.log('First Word: ' + satu); 
    console.log('Second Word: ' + dua); 
    console.log('Third Word: ' + tiga); 
    console.log('Fourth Word: ' + empat); 
    console.log('Fifth Word: ' + lima); 
    console.log('Sixth Word: ' + enam); 
    console.log('Seventh Word: ' + tujuh); 
    console.log(`Eighth Word: ${delapan} \n`);
    console.log("---------NO 3----------")
    }
    aksesChar();
    
    //Soal No. 3 Mengurai Kalimat (Substring)
    const aksesSubs = () => {
        var sentence2 = 'wow JavaScript is so cool'; 
    
        var satu = sentence2.substring(0, 3); 
        var dua = sentence2.substring(4, 14);
        var tiga = sentence2.substring(15, 17); 
        var empat = sentence2.substring(18, 20);
        var lima = sentence2.substring(21, 25);
        
        console.log('First Word: ' + satu); 
        console.log('Second Word: ' + dua); 
        console.log('Third Word: ' + tiga); 
        console.log('Fourth Word: ' + empat);
        console.log(`Fifth Word: ${lima}\n`);
        console.log("---------NO 4----------")
        
    }
    aksesSubs();
    
    //Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
    const aksesSubsLeng = () => {
        var sentence2 = 'wow JavaScript is so cool'; 
    
        var satu = sentence2.substring(0, 3); 
        var dua = sentence2.substring(4, 14);
        var tiga = sentence2.substring(15, 17); 
        var empat = sentence2.substring(18, 20);
        var lima = sentence2.substring(21, 25);
        
        console.log('First Word: ' + satu + ', with length: ' + satu.length ); 
        console.log('Second Word: ' + dua + ', with length: ' + dua.length); 
        console.log('Third Word: ' + tiga + ', with length: ' + tiga.length); 
        console.log('Fourth Word: ' + empat + ', with length: ' + empat.length); 
        console.log(`Fifth Word: ${lima}, with length: ${lima.length}\n`);
        
    }
    aksesSubsLeng();
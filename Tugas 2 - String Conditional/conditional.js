//if-else
console.log(`-----IF ELSE-----\n`)

const werewolf = (nama, peran) => {
    
    if ( nama == "" && peran == "") {
        console.log(`Nama dan peran harus diisi! \n`);
    } else if( nama == "") {
        console.log(`Nama harus diisi! \n`);
    }
    else if ( peran == "penyihir") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf! \n`);
    } else if( peran == "guard") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf. \n`);
    }
    else if( peran == "werewolf") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam! \n`);
    } else {
        console.log("Silahkan pilih peran anda!")
        console.log(`Terdapat peran: penyihir,guard,werewolf \n`)
    }
    
}
werewolf(
   /* Isi Nama Anda */  "taufik", 
  /* Isi Peran Anda */  "werewolf");
console.log(`-----SWITCH CASE-----\n`)


  //Switch Case
var hari = 2;
if ( hari > 31 || hari <= 0) {
    hari = "( 1 - 31 )";
    console.log(`Masukan hari antara 1-31\n`)
}
var bulan = 5;
if ( bulan > 12 || bulan <= 0) {
    bulan = "( 1 - 12 )";
    console.log(`Masukan bulan antara 1-12\n`)
}
var tahun = 2001;
if ( tahun > 2200 || tahun <= 1899) {
    tahun = "( 1900 - 2200 )";
    console.log(`Masukan tahun antara 1900-2200\n`)
}
switch(bulan) {
    case 1:   { bulan = "Januari"; break; }
    case 2:   { bulan = "Februari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break; }
    case 10:  { bulan = "Oktober"; break; }
    case 11:  { bulan = "November"; break; }
    case 12:  { bulan = "Desember"; break; }}




    console.log(`${hari} ${bulan} ${tahun}`);

//1. Mengubah fungsi menjadi fungsi arrow
console.log(`-------NO 1--------`)
const golden = () => {
    console.log("this is golden!!");
}
golden();
console.log(`\n-------NO 2--------`)

//2. Sederhanakan menjadi Object literal di ES6
const newFunction = function literal(firstName, lastName){
    return {
firstName,
lastName,
fullName: function(){
        console.log(firstName + " " + lastName)
        return 
}
}
}
newFunction("William", "Imoh").fullName() 



//3. Destructuring
console.log(`\n-------NO 3--------`)
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation)

//4. Array Spreading
console.log(`\n-------NO 4--------`)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

//5. Template Literals
console.log(`\n-------NO 5--------`)
const planet = "earth"
const view = "glass"
var before = (`Lorem view dolor sit amet, consectetur adipiscing elit, planetdo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`)
console.log(before) 